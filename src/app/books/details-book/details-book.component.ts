import { BooksService } from "./../books.service";
import { Book } from "./../book.model";
import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";

@Component({
  selector: "app-details-book",
  templateUrl: "./details-book.component.html",
  styleUrls: ["./details-book.component.sass"],
})
export class DetailsBookComponent implements OnInit {
  @Input() bookId: string;

  book: Book;

  constructor(
    private booksService: BooksService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.url.subscribe((data) => {
      console.log(data);
    });

    this.activatedRoute.paramMap.subscribe((params) => {
      console.log(params);

      this.bookId = params.get("idBook");

      this.booksService
        .findBookById(this.bookId.toString())
        .subscribe((response) => {
          console.log("data from");

          console.log(response);
          this.book = response as Book;
        });
    });
  }
}
