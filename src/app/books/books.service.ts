import { Book } from "./book.model";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class BooksService {
  private selectedBook: Book;

  constructor(private httpClient: HttpClient) {}

  public setBook(book: Book) {
    this.selectedBook = book;

    console.log("set book");
  }

  public getBook(): Book {
    return this.selectedBook;
  }

  public addBook(book: Book) {
    this.httpClient
      .post("http://localhost:8080/books", book)
      .subscribe((response) => {
        console.log(response);
      });
  }

  public editBook(book: Book) {
    this.httpClient
      .put(`http://localhost:8080/books?id=${book.id}`, book)
      .subscribe((response) => {
        console.log(response);
      });
  }

  public deleteBook(book: Book) {
    this.httpClient
      .delete(`http://localhost:8080/books?id=${book.id}`)
      .subscribe((response) => {
        console.log(response);
      });
  }

  public findBookById(id: string) {
    return this.httpClient.get(`http://localhost:8080/books/byId?id=${id}`);
  }

  public findBookByAuthor(author: string) {
    return this.httpClient.get(
      `http://localhost:8080/books/byId?author=${author}`
    );
  }

  public findBookByDescription(description: string) {
    return this.httpClient.get(
      `http://localhost:8080/books/byId?description=${description}`
    );
  }

  public findBookByTitle(title: string) {
    return this.httpClient.get(
      `http://localhost:8080/books/byId?title=${title}`
    );
  }

  public getBooks() {
    return this.httpClient.get("http://localhost:8080/books");
  }
}
