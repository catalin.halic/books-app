import { BooksService } from "./books.service";
import { Component, OnInit } from "@angular/core";
import { Book } from "./book.model";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";

@Component({
  selector: "app-books",
  templateUrl: "./books.component.html",
  styleUrls: ["./books.component.sass"],
})
export class BooksComponent implements OnInit {
  books: Array<Book> = [];
  isActive: boolean = false;

  selectedBook: Book = null;

  value = "Clear me";

  constructor(
    private booksService: BooksService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe((queryParams: ParamMap) => {
      console.log("query param map");
      console.log(queryParams);
    });
  }

  getBooks() {
    this.booksService.getBooks().subscribe((response) => {
      console.log(response);

      this.books = response as Array<Book>;
    });
  }

  onDelete(book: Book, event) {
    this.booksService.deleteBook(book);

    // opreste propagarea click-ului
    event.stopPropagation();

    this.getBooks();
  }

  onEdit(book: Book) {
    this.isActive = true;

    this.selectedBook = book;
  }

  onAddBook() {
    this.isActive = true;
  }

  onStatusResponse(event) {
    console.log(event);
  }

  openBookDetails(idBook: string, bookName: string) {
    this.router.navigate(["/", "book-details", idBook, bookName]);
  }
}
