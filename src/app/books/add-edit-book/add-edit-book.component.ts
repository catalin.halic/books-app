import { BooksService } from "./../books.service";
import { Book } from "./../book.model";
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-add-edit-book",
  templateUrl: "./add-edit-book.component.html",
  styleUrls: ["./add-edit-book.component.sass"],
})
export class AddEditBookComponent implements OnInit, OnChanges {
  @Input() book: Book;
  @Output() statusRequest: EventEmitter<any> = new EventEmitter();

  formBook: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private booksService: BooksService
  ) {}

  ngOnChanges() {
    console.log(this.book);

    this.setupBookForm();
  }

  ngOnInit(): void {
    this.setupBookForm();
  }
  onSubmit() {
    console.log(this.formBook.value);

    if (this.formBook.get("id").value == null) {
      this.booksService.addBook(this.formBook.value);

      this.statusRequest.emit("Carte adaugata cu succes!");
    } else {
      this.booksService.editBook(this.formBook.value);

      this.statusRequest.emit("Carte actualizata cu succes!");
    }

    this.book = null;

    this.setupBookForm();
  }
  setupBookForm() {
    if (this.book != null) {
      this.formBook = this.formBuilder.group({
        id: [this.book.id],
        title: [this.book.title],
        author: [this.book.author],
        description: [this.book.description],
      });
    } else {
      this.formBook = this.formBuilder.group({
        id: [null],
        title: [],
        author: [],
        description: [],
      });
    }
  }
}
