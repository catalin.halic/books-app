import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BooksComponent } from "./books/books.component";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SearchBookComponent } from "./books/search-book/search-book.component";
import { DetailsBookComponent } from "./books/details-book/details-book.component";
import { AddEditBookComponent } from "./books/add-edit-book/add-edit-book.component";
import { AuthComponent } from "./auth/auth.component";
import { DatePipe } from "@angular/common";
import { TestPipe } from "./test.pipe";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    SearchBookComponent,
    DetailsBookComponent,
    AddEditBookComponent,
    AuthComponent,
    TestPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
