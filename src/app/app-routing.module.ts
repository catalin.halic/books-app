import { DetailsBookComponent } from "./books/details-book/details-book.component";
import { BooksComponent } from "./books/books.component";
import { AuthComponent } from "./auth/auth.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/books-sda", pathMatch: "full" },
  { path: "auth", component: AuthComponent },
  { path: "books-sda", component: BooksComponent },
  { path: "book-details/:idBook/:bookName", component: DetailsBookComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
