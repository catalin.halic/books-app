import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "test",
})
export class TestPipe implements PipeTransform {
  transform(value: String, ...args: any[]): String {
    console.log("test pipe transform method");

    console.log(args);

    console.log(value);

    return value.replace("mere", "pere");
  }
}
