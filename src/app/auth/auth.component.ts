import { AuthService } from "./auth.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavigationExtras, Router } from "@angular/router";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.component.html",
  styleUrls: ["./auth.component.scss"],
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;

  authType: string = "login";

  dateTime: Date;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.setupLoginForm();
    this.setupRegisterForm();

    this.dateTime = new Date();
  }

  onAuthTypeChange(): void {
    switch (this.authType) {
      case "login":
        this.authType = "register";
        break;
      case "register":
        this.authType = "login";
        break;
    }
  }

  onLogin(): void {
    console.log("Date login");
    console.log(this.loginForm.value);

    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe((response) => {
        console.log(response);

        var navigationExtras: NavigationExtras = {
          queryParams: {
            username: this.loginForm.get("username").value,
            password: this.loginForm.get("password").value,
          },
        };

        this.router.navigate(["/", "books-sda"], navigationExtras);
      });

      this.authType = "";
    } else {
      console.log("formularul este invalid");
    }
  }

  onRegister(): void {
    // varianta 1
    var password: String = this.registerForm.get("password").value;
    var confirmPassword: String = this.registerForm.get("confirmPassword")
      .value;

    if (password == confirmPassword) {
      console.log(this.registerForm.value);

      this.authService.register(this.registerForm.value);
    } else {
      console.log("Parolele nu se potrivesc!");
    }
    return;
    // varianta 2
    if (
      this.registerForm.get("password").value ==
      this.registerForm.get("confirmPassword").value
    ) {
      console.log(this.registerForm.value);

      this.authService.register(this.registerForm.value);
    } else {
      console.log("Parolele nu se potrivesc!");
    }
  }

  private setupLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.compose([Validators.required])],
      password: ["", Validators.minLength(6)],
    });
  }

  private setupRegisterForm(): void {
    this.registerForm = this.formBuilder.group({
      username: [],
      password: [],
      confirmPassword: [],
    });
  }
}
