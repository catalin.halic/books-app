import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  public login(loginData: any) {
    return this.httpClient.post("http://localhost:8080/auth/login", loginData);
  }

  public register(registerData: any) {
    this.httpClient
      .post("http://localhost:8080/auth/register", registerData)
      .subscribe((response) => {
        console.log(response);
      });
  }
}
